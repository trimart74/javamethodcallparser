
import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.github.javaparser.symbolsolver.utils.SymbolSolverCollectionStrategy;
import com.github.javaparser.utils.ProjectRoot;
import com.github.javaparser.utils.SourceRoot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private static final String SRC = "C:\\Users\\Mikhail.Terpugov\\IdeaProjects\\keycloak\\";
    private static final String FILE_PATH = "C:\\Users\\Mikhail.Terpugov\\IdeaProjects\\keycloak\\services\\src\\main\\java\\org\\keycloak\\services\\scheduled\\DeactivationUsers.java";
    private static final String JAR_PATH = "C:\\Users\\Mikhail.Terpugov\\.m2\\repository\\javax\\ws\\rs\\jsr311-api\\1.1.1\\jsr311-api-1.1.1.jar";
    private static List<String> direcoryPath = new ArrayList<>();
    private static VoidVisitor<Map<String, MethodDeclaration>> methodNameCollector = new MethodNameCollector();
    private static CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
    private static Map<String, CompilationUnit> classToCompilationUnitMap = new HashMap<>();
    private static Map<String, MethodDeclaration> methodNames = new HashMap<>();
    private static List<MethodDeclaration> listOfMethodOfChildClass = new ArrayList<>();


    public static void main(String[] args) throws Exception {
        TypeSolver typeSolver = new CombinedTypeSolver();
        TypeSolver javaParserTypeSolver = new JavaParserTypeSolver(new File(SRC));


        JarTypeSolver jarTypeSolver = new JarTypeSolver(new File(JAR_PATH));
        JarTypeSolver jarTypeSolver1 = new JarTypeSolver(new File(SRC + "\\keycloak-11.0.2\\bin\\client\\keycloak-admin-cli-11.0.2.jar"));
        JarTypeSolver jarTypeSolver4 = new JarTypeSolver(new File(SRC + "distribution/server-dist/target/keycloak-11.0.2/modules/system/layers/keycloak/org/keycloak/keycloak-services/main/keycloak-services-11.0.2.jar"));
        JarTypeSolver jarTypeSolver5 = new JarTypeSolver(new File(SRC + "model\\jpa\\target\\keycloak-model-jpa-11.0.2.jar"));
        JarTypeSolver jarTypeSolver6 = new JarTypeSolver(new File(SRC + "authz\\client\\target\\keycloak-authz-client-11.0.2.jar"));
        JarTypeSolver jarTypeSolver7 = new JarTypeSolver(new File(SRC + "authz/policy/common/target/keycloak-authz-policy-common-11.0.2.jar"));
        JarTypeSolver jarTypeSolver8 = new JarTypeSolver(new File(SRC + "common/target/keycloak-common-11.0.2.jar"));
        JarTypeSolver jarTypeSolver9 = new JarTypeSolver(new File(SRC + "core/target/keycloak-core-11.0.2.jar"));
        JarTypeSolver jarTypeSolver11 = new JarTypeSolver(new File(SRC + "federation/ldap/target/keycloak-ldap-federation-11.0.2.jar"));
        JarTypeSolver jarTypeSolver12 = new JarTypeSolver(new File(SRC + "federation/sssd/target/keycloak-sssd-federation-11.0.2.jar"));
        JarTypeSolver jarTypeSolver13 = new JarTypeSolver(new File(SRC + "federation/sssd/target/keycloak-sssd-federation-11.0.2.jar"));
        JarTypeSolver jarTypeSolver14 = new JarTypeSolver(new File(SRC + "integration/admin-client/target/keycloak-admin-client-11.0.2.jar"));
        JarTypeSolver jarTypeSolver16 = new JarTypeSolver(new File(SRC + "integration/client-cli/admin-cli/target/keycloak-admin-cli-11.0.2.jar"));
        JarTypeSolver jarTypeSolver17 = new JarTypeSolver(new File(SRC + "integration/client-cli/client-registration-cli/target/keycloak-client-registration-cli-11.0.2.jar"));
        JarTypeSolver jarTypeSolver18 = new JarTypeSolver(new File(SRC + "integration/client-registration/target/keycloak-client-registration-api-11.0.2.jar"));
        JarTypeSolver jarTypeSolver19 = new JarTypeSolver(new File(SRC + "ismodule/target/ismodule.jar"));
        JarTypeSolver jarTypeSolver20 = new JarTypeSolver(new File(SRC + "model/infinispan/target/keycloak-model-infinispan-11.0.2.jar"));
        JarTypeSolver jarTypeSolver21 = new JarTypeSolver(new File(SRC + "model/jpa/target/keycloak-model-jpa-11.0.2.jar"));
        JarTypeSolver jarTypeSolver22 = new JarTypeSolver(new File(SRC + "model/map/target/keycloak-model-map-11.0.2.jar"));
        JarTypeSolver jarTypeSolver23 = new JarTypeSolver(new File(SRC + "model/map/target/keycloak-model-map-11.0.2.jar"));
        JarTypeSolver jarTypeSolver24 = new JarTypeSolver(new File(SRC + "saml-core/target/keycloak-saml-core-11.0.2.jar"));
        JarTypeSolver jarTypeSolver25 = new JarTypeSolver(new File(SRC + "saml-core-api/target/keycloak-saml-core-public-11.0.2.jar"));
        JarTypeSolver jarTypeSolver26 = new JarTypeSolver(new File(SRC + "server-spi/target/keycloak-server-spi-11.0.2.jar"));
        JarTypeSolver jarTypeSolver27 = new JarTypeSolver(new File(SRC + "server-spi-private/target/keycloak-server-spi-private-11.0.2.jar"));
        JarTypeSolver jarTypeSolver28 = new JarTypeSolver(new File(SRC + "util/embedded-ldap/target/keycloak-util-embedded-ldap-11.0.2.jar"));
        JarTypeSolver jarTypeSolver29 = new JarTypeSolver(new File(SRC + "util/embedded-ldap/target/keycloak-util-embedded-ldap-11.0.2.jar"));
        combinedTypeSolver.add(typeSolver);
        combinedTypeSolver.add(javaParserTypeSolver);
        combinedTypeSolver.add(jarTypeSolver);
        combinedTypeSolver.add(jarTypeSolver1);
        combinedTypeSolver.add(jarTypeSolver4);
        combinedTypeSolver.add(jarTypeSolver5);
        combinedTypeSolver.add(jarTypeSolver6);
        combinedTypeSolver.add(jarTypeSolver7);
        combinedTypeSolver.add(jarTypeSolver8);
        combinedTypeSolver.add(jarTypeSolver9);
        combinedTypeSolver.add(jarTypeSolver11);
        combinedTypeSolver.add(jarTypeSolver12);
        combinedTypeSolver.add(jarTypeSolver13);
        combinedTypeSolver.add(jarTypeSolver14);
        combinedTypeSolver.add(jarTypeSolver16);
        combinedTypeSolver.add(jarTypeSolver17);
        combinedTypeSolver.add(jarTypeSolver18);
        combinedTypeSolver.add(jarTypeSolver19);
        combinedTypeSolver.add(jarTypeSolver20);
        combinedTypeSolver.add(jarTypeSolver21);
        combinedTypeSolver.add(jarTypeSolver22);
        combinedTypeSolver.add(jarTypeSolver23);
        combinedTypeSolver.add(jarTypeSolver24);
        combinedTypeSolver.add(jarTypeSolver25);
        combinedTypeSolver.add(jarTypeSolver26);
        combinedTypeSolver.add(jarTypeSolver27);
        combinedTypeSolver.add(jarTypeSolver28);
        combinedTypeSolver.add(jarTypeSolver29);
        combinedTypeSolver.add(new ReflectionTypeSolver(true));
        Path projectRoot = Paths.get(SRC);

        ProjectRoot projectRoot1 = new SymbolSolverCollectionStrategy().collect(projectRoot);
        for (SourceRoot s : projectRoot1.getSourceRoots()) {
            try {
                for (ParseResult<CompilationUnit> parseResult : s.tryToParse()) {
                    if (parseResult.getResult().isPresent()) {
                        CompilationUnit cu = parseResult.getResult().get();
                        String className = "";
                        if (cu.getTypes().size() > 0) {
                            className = cu.getTypes().get(0).getFullyQualifiedName().get();

                        }
                        classToCompilationUnitMap.put(className, cu);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        HashMap<String, Set<String>> classAndHisMethods = new HashMap<>();
        Set<String> methods = new HashSet<>();
        methods.add("setupScheduledTasks");
        classAndHisMethods.put("org.keycloak.services.resources.KeycloakApplication", methods);
        int n = 0;
        while (n < 3) {
            n++;
            if (TypeCalculatorVisitor.classToMethod.size() > 0) {
                classAndHisMethods = new HashMap<>(TypeCalculatorVisitor.classToMethod);
            }
            HashMap<String, Set<String>> result = getListOfClassesFromMethodCall(classAndHisMethods);
            if (result.size() > 0) {
                getListOfClassesFromMethodCall(result);
            }
        }

        Set<String> filtred = TypeCalculatorVisitor.getResult().stream().filter(el -> el.startsWith("org")).collect(Collectors.toSet());
        System.out.println(filtred.size() + "---> size");
        System.out.println("класссы:");
        for (String s : filtred) {
            System.out.println(s);
            String classNameWithoutGeneric = getClassNameWithoutGeneric(s);
            CompilationUnit cu = classToCompilationUnitMap.get(classNameWithoutGeneric);
            if (cu != null && cu.getStorage().isPresent()) {

                String path = classToCompilationUnitMap.get(classNameWithoutGeneric).getStorage().get().getDirectory() + "\\" + getClassName(classNameWithoutGeneric);
                direcoryPath.add(path);
            }
        }

        System.out.println("диркетории:");


        for (String s : direcoryPath) {
            int indexOfrelativePath = s.indexOf("\\keycloak\\");
            String relativePath = s.substring(indexOfrelativePath + "\\keycloak\\".length());
            System.out.println(relativePath);
        }

    }

    private static String getClassNameWithoutGeneric(String s) {
        int indexOfGeneric = s.indexOf("<");
        String classNameWithoutGeneric = "";
        if (indexOfGeneric > 0) {
            classNameWithoutGeneric = s.substring(0, indexOfGeneric);
        } else {
            classNameWithoutGeneric = s;
        }
        return classNameWithoutGeneric;
    }

    private static HashMap<String, Set<String>> getListOfClassesFromMethodCall(HashMap<String, Set<String>> classAndHisMethod) {
        HashMap<String, Set<String>> classAndHisMethod1 = new HashMap<>();
        for (Map.Entry<String, Set<String>> entry : classAndHisMethod.entrySet()) {
            for (String classTo : entry.getValue()) {
                System.out.println(entry.getKey() + "." + classTo);
                CompilationUnit cu = classToCompilationUnitMap.get(entry.getKey());
                if (cu != null) {
                    ClassOrInterfaceDeclaration topLevelClassDec = cu.getLocalDeclarationFromClassname(entry.getKey()).get(0);
                    for (Map.Entry<String, CompilationUnit> aClass : classToCompilationUnitMap.entrySet()) {
                        List<ClassOrInterfaceDeclaration> classOrInterfaceDeclarationList = null;
                        try {
                            classOrInterfaceDeclarationList = aClass.getValue().getLocalDeclarationFromClassname(aClass.getKey());
                        } catch (Exception e) {
                            System.out.println("Class not faund! " + aClass.getKey() + " " + classTo);
                        }
                        if (classOrInterfaceDeclarationList != null && classOrInterfaceDeclarationList.size() > 0) {
                            List<ClassOrInterfaceType> some = aClass.getValue().getLocalDeclarationFromClassname(aClass.getKey()).get(0).getImplementedTypes();
                            List<ClassOrInterfaceType> some1 = aClass.getValue().getLocalDeclarationFromClassname(aClass.getKey()).get(0).getExtendedTypes();
                            some.addAll(some1);
                            long implemented = some.stream().filter(clOrIn -> clOrIn.getName().equals(topLevelClassDec.getName())).count();
                            if (implemented > 0) {
                                System.out.println("DesencantOf " + topLevelClassDec.getNameAsString() + " is " + aClass.getKey());
                                TypeCalculatorVisitor.result.add(aClass.getKey());
                                Set<String> setOfMethods = new HashSet<>();
                                setOfMethods.add(classTo);
                                for (ClassOrInterfaceType classOrInterfaceType : some) {
                                    if (classAndHisMethod1.get(classOrInterfaceType.getNameAsString()) != null && !classAndHisMethod1.get(classOrInterfaceType.getNameAsString()).isEmpty()) {
                                        Set<String> methods = classAndHisMethod1.get(classOrInterfaceType.getNameAsString());
                                        methods.addAll(setOfMethods);
                                        classAndHisMethod1.put(aClass.getKey(), methods);
                                    } else {
                                        classAndHisMethod1.put(aClass.getKey(), setOfMethods);
                                    }
                                }
                                String className = aClass.getKey().substring(aClass.getKey().lastIndexOf(".") + 1);
                                if (aClass.getValue().getClassByName(className).isPresent()) {
                                    listOfMethodOfChildClass.addAll(aClass.getValue().getClassByName(className).get().getMethodsByName(classTo));
                                }
                            }
                        }
                    }

                    methodNameCollector.visit(cu, methodNames);
                    MethodDeclaration md = methodNames.get(classTo);
                    for (MethodDeclaration md1 : listOfMethodOfChildClass) {
                        md1.accept(new TypeCalculatorVisitor(), JavaParserFacade.get(combinedTypeSolver));
                    }
                    if (md != null) {
                        md.accept(new TypeCalculatorVisitor(), JavaParserFacade.get(combinedTypeSolver));
                    }
                }
            }
        }
        return classAndHisMethod1;
    }

    private static class MethodNameCollector extends VoidVisitorAdapter<Map<String, MethodDeclaration>> {

        @Override
        public void visit(MethodDeclaration md, Map<String, MethodDeclaration> collector) {
            super.visit(md, collector);
            if (md != null) {
                collector.put(md.getNameAsString(), md);
            }
        }
    }

    static class TypeCalculatorVisitor extends VoidVisitorAdapter<JavaParserFacade> {

        private static Set<String> result = new HashSet<>();
        private static HashMap<String, Set<String>> classToMethod = new HashMap();

        public static Set<String> getResult() {
            return result;
        }

        public static void setResult(Set<String> result) {
            TypeCalculatorVisitor.result = result;
        }

        @Override
        public void visit(MethodDeclaration n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);

            try {
                String type = javaParserFacade.getType(n).describe();
                if (type.startsWith("org.keyckloak")) {
                    result.add(type);
                }
            } catch (Exception e) {
            }

        }

        @Override
        public void visit(ReturnStmt n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);
            try {
                String type = javaParserFacade.getType(n).describe();
                if (type.startsWith("org.keyckloak")) {
                    result.add(type);
                }
            } catch (Exception e) {

            }
        }

        @Override
        public void visit(FieldAccessExpr n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);

            try {
                System.out.println(n.toString() + "fac has type " + javaParserFacade.getType(n).describe());
                String type = javaParserFacade.getType(n).describe();

                if (type.startsWith("org.keycloak")) {
                    result.add(type);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void visit(NameExpr n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);
            try {
                System.out.println(n.toString() + "namedExpr has type " + javaParserFacade.getType(n).describe());
                String type = javaParserFacade.getType(n).describe();
                if (type.startsWith("org.keycloak")) {
                    result.add(type);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }

        @Override
        public void visit(MethodCallExpr n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);
            try {
                System.out.print(n.toString() + " --> ");
                System.out.println(" has type " + javaParserFacade.getType(n).describe());
                String callerClass = javaParserFacade.getType(n.getScope().get()).describe();
                result.add(javaParserFacade.getType(n).describe());
                String type = javaParserFacade.getType(n).describe();
                String callerClassWithoutGeneric = getClassNameWithoutGeneric(callerClass);
                if (type.startsWith("org.keycloak")) {
                    if (classToMethod.containsKey(callerClass)) {
                        classToMethod.get(callerClassWithoutGeneric).add(n.getNameAsString());
                    } else {
                        Set<String> methodNames = new HashSet<>();
                        methodNames.add(n.getNameAsString());
                        classToMethod.put(callerClassWithoutGeneric, methodNames);
                    }
                    result.add(type);
                }
            } catch (Exception e) {

            }
        }
    }

    private static String getClassName(String classNameWithPackage) {
        int indexOfLastPoint = classNameWithPackage.lastIndexOf(".");
        String className = classNameWithPackage.substring(indexOfLastPoint + 1);
        return className;
    }
}
